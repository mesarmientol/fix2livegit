﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{

    
    public int i;
    public GameObject[] items;
    public GameObject seleccionado;

    public int scroll;


    // Start is called before the first frame update
    void Start()
    {
        items = new GameObject[3];
        i = 0;
        scroll = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
        Scroll();


    }
    public void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Recolectar")
        {
            Debug.Log("Holi");
            items[i] = other.gameObject;
            i++;
            other.gameObject.GetComponent<BoxCollider>().enabled=false;
            other.gameObject.GetComponent<MeshRenderer>().enabled = false;
            //other.gameObject.SetActive(false);
            

        }

    }
    public void Scroll() {
        if (scroll<0) {
            scroll = 0;
        }
        if (scroll > 3)
        {
            scroll = 3;
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            scroll++;
             
           
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            scroll--;
        }
        seleccionado = items[scroll];
    }

}
