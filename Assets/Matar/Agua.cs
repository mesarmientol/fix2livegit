﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agua : MonoBehaviour
{
    // Start is called before the first frame update
    
    public float speed;
    Vector3 pos = Vector3.zero;
    public bool subir;
    public Animator animator;
    void Start()
    {
        subir = true;
        pos = new Vector3(transform.position.x , transform.position.y, transform.position.z);
        animator.enabled = false;
    }


    // Update is called once per frame
    void Update()
    {
        if (subir)
        {

            gameObject.transform.localPosition = pos;
            pos.y = pos.y + 0.0005f;
        }
        else {
            pos.y = pos.y +0 ;
        }


        if(pos.y > 139f)
        {
            animator.enabled = true;
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Electricidad") {
            subir = false;
        }
    }

}
