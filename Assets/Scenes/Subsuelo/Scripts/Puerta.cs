﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public bool abrir;
    public GameObject personaje;
    bool animaciones;
    // Start is called before the first frame update
    void Start()
    {
        abrir = false;
    }

    // Update is called once per frame
    void Update()
    {

        abrir = personaje.GetComponent<PersonajeMovimiento>().activadorPuertal1;
        if (animaciones&& this.gameObject.transform.localEulerAngles.y < 90) {
            this.gameObject.transform.localEulerAngles = new Vector3(this.gameObject.transform.localEulerAngles.x, this.gameObject.transform.localEulerAngles.y + 1, this.gameObject.transform.localEulerAngles.z);

        }

    }
    public void OnTriggerStay(Collider other)
    {
        if (abrir == false)
        {
            Debug.Log("Pueta bloqueada");
        }
        else {

            if (abrir && this.gameObject.transform.localEulerAngles.y < 90&& Input.GetKeyUp(KeyCode.E))
            {
                animaciones = true;
            }
        }

    }
}
