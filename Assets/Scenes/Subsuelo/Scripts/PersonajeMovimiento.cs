﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonajeMovimiento : MonoBehaviour
{


    public int i;
    public GameObject[] items;
    public GameObject seleccionado;
    public bool activadorPuertal1;
    public bool roto = false;

    public GameObject invenT1;
    public GameObject invent2;

    public Text numInv;

    float x, y;

    public int scroll;
    public Animator ani;

    // Start is called before the first frame update
    void Start()
    {

        invenT1.SetActive(true);
        invent2.SetActive(false);



        items = new GameObject[3];
        i = 0;
        scroll = 0;
        activadorPuertal1 = false;

        x = 0;
        y=0;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //Para poder agregar en el inventario

        if (seleccionado)
        {
            invenT1.SetActive(false);
            invent2.SetActive(true);
        }






        //  x = Input.GetAxis("Horizontal");
        // y = Input.GetAxis("Vertical");
        //ani.SetFloat("velX", x);
        //ani.SetFloat("velY", y);

        ani.SetFloat("velY", x);
        if (Input.GetKey(KeyCode.W))
        {
            x = 3;
            ani.SetFloat("velY", x);
        }
        else {
            x = 0;
        }
        if (Input.GetKey(KeyCode.S))
        {
            x = -3;
            ani.SetFloat("velY", x);
        }
        else
        {
            x = 0;
        }


        if (Input.GetKey(KeyCode.W)&& Input.GetKey(KeyCode.LeftShift))
        {
            x = 4;
            ani.SetFloat("velY", x);
        }
        else
        {
            x = 0;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            x = 2;
            ani.SetFloat("velY", x);
        }
        else
        {
            x = 0;
        }



        Scroll();
        

    }
    
    public void OnTriggerStay(Collider other)
    {
        Debug.Log("Presiona E para recoger");
        if (Input.GetKeyUp(KeyCode.E))
        {

            if (other.gameObject.tag == "Recolectar")
            {
                items[i] = other.gameObject;
                i++;
                numInv.text = i.ToString();




                if (i == 3)
                {
                    activadorPuertal1 = true;
                    i = 1;
                }


                if (i == 1)
                {
                    seleccionado = other.gameObject;
                    roto = true;
                }
                other.gameObject.GetComponent<BoxCollider>().enabled = false;
                other.gameObject.GetComponent<BoxCollider>().enabled = false;
                other.gameObject.SetActive(false);



            }
        }
        

    }

    public void Scroll()
    {
        if (scroll < 0)
        {
            scroll = 0;
        }
        if (scroll > 3)
        {
            scroll = 3;
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            scroll++;

        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            scroll--;
        }
        if (items[scroll]!=null) {
            seleccionado = items[scroll];
        }
        else {
            seleccionado = items[0];
        }
        
    }

}
